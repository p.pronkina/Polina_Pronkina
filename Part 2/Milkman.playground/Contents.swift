// Задача 1
var milkmanPhrase = "Молоко - это полезно!"

print(milkmanPhrase)


// Задача 2, 3
var milkPrice: Double = 3
milkPrice = 4.20


// Задача 4 + *
var milkBottleCount: Int? = 20
var profit: Double = 0.0
profit = milkPrice * Double(milkBottleCount!)

//if let check = milkBottleCount {
//   profit = milkPrice * Double(check)
//} else {
//    print("Нет бутылок")
//}

// Принудительное развертывание может привести к падению приложения, если делать его небезопасно (первый незакоменченный способ), так как мы не делаем проверку на nil.
// Пример: между строками 13 и 15 переменной milkBottleCount было присвоено значение nil

print(profit)


// Задача 5
var employeesList: Array<String> = []
employeesList = ["Петр", "Геннадий", "Иван", "Марфа", "Андрей"]
print (employeesList)


// Задача 6
var isEveryoneWorkHard: Bool = false
var workingHours = 40
//var workingHours = 25

if workingHours >= 40 {
    isEveryoneWorkHard = true
} else {
    isEveryoneWorkHard = false
}

print(isEveryoneWorkHard)
