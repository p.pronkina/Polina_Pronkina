import Foundation

func fibonacciNumber(count: Int) -> Array<Int> {
    var storyPointsNumbers: Array<Int> = []
    var number1 = 1
    var number2 = 1
    storyPointsNumbers.append(number1)
    storyPointsNumbers.append(number2)
    
    for _ in 0 ..< count - 2 {
        let number = number1 + number2
        number1 = number2
        number2 = number
        storyPointsNumbers.append(number)
    }
    
    return storyPointsNumbers
}

print(fibonacciNumber(count: 5))

print(fibonacciNumber(count: 7))
